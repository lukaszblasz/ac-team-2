import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import FlowersList from './components/FlowersList'

import './App.scss';
import flowers from './flowers.json'

class App extends Component {
  state = {
    flowersList: flowers.list
  }

  render() {
    const { flowersList } = this.state;
    return (
      <Router>
        <Route 
        path="/" 
        exact
        render={(props) => <FlowersList {...props} flowers={this.state.flowersList} />}
        />
      </Router>
    );
  }
}

export default App;
